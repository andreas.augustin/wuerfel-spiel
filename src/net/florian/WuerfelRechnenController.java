package net.florian;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class WuerfelRechnenController {

    private Random zufall;
    public static final int STANDARD_BREITE=6;
    public static final int STANDARD_HOEHE=8;
    public static final int START_WERT=1;
    public static final Farbe START_FARBE=Farbe.GRUEN;

    private Feld[][] zahlenFeld;

    private Grenzlinie[][] horizontale_grenzen;
    private Grenzlinie[][] vertikale_grenzen;

    private Farbe aktuelleFarbe;
    private int reihenfolge;

    public WuerfelRechnenController() {
        this.zahlenFeld = new Feld[STANDARD_BREITE][STANDARD_HOEHE];
        this.horizontale_grenzen = new Grenzlinie[STANDARD_BREITE][STANDARD_HOEHE+1];
        this.vertikale_grenzen = new Grenzlinie[STANDARD_BREITE+1][STANDARD_HOEHE];
        this.aktuelleFarbe = START_FARBE;
        this.zufall = new Random();
        this.reihenfolge = START_WERT;
    }

    public void aendereAktuelleFarbe() {
        switch ( aktuelleFarbe ){
            case GRUEN: this.aktuelleFarbe = Farbe.ROT; break;
            case ROT: this.aktuelleFarbe = Farbe.GRUEN; break;
        }
        reihenfolge++;
    }
    public Farbe gibAktuelleFarbe() {
        return aktuelleFarbe;

    }



    public static final int[] DOPPEL_WUERFEL = {1,2,3,4,5,6,8,9,10,12,15,16,18,20,24,30,36};


    public boolean gueltigeWuerfelZahl( int wuerfel ) {
        for( int i=0;i < DOPPEL_WUERFEL.length; i++ ) {
            if( DOPPEL_WUERFEL[i] == wuerfel ) { return true; }
        }
        return false;
    }

    public void initialisiereSpielfeld() {

        for( int y=0; y < STANDARD_HOEHE; y++) {
            for ( int x=0; x < STANDARD_BREITE; x++ ) {
                this.zahlenFeld[x][y] = new Feld( zufall.nextInt(), x,y, zufallsDoppelWuerfel() );
            }
        }


        // vertikale grenzen
        for( int y=0; y < STANDARD_HOEHE; y++) {
            for ( int x=0; x < STANDARD_BREITE+1; x++ ) {
                if( x == 0 ) {
                    vertikale_grenzen[x][y] = new Grenzlinie(zufall.nextInt(),null,zahlenFeld[x][y]);
                } else {
                    if ( x == STANDARD_BREITE) {
                        vertikale_grenzen[x][y] = new Grenzlinie(zufall.nextInt(),zahlenFeld[x-1][y],null);
                    } else {
                        vertikale_grenzen[x][y] = new Grenzlinie(zufall.nextInt(),zahlenFeld[x-1][y],zahlenFeld[x][y]);
                    }
                }

            }
        }
        // horizontale grenzen
        for( int y=0; y < STANDARD_HOEHE+1; y++) {
            for ( int x=0; x < STANDARD_BREITE; x++ ) {
                if( y == 0 ) {
                    horizontale_grenzen[x][y] = new Grenzlinie(zufall.nextInt(),null,zahlenFeld[x][y]);
                } else {
                    if ( y == STANDARD_HOEHE) {
                        horizontale_grenzen[x][y] = new Grenzlinie(zufall.nextInt(),zahlenFeld[x][y-1],null);
                    } else {
                        horizontale_grenzen[x][y] = new Grenzlinie(zufall.nextInt(),zahlenFeld[x][y-1],zahlenFeld[x][y]);
                    }
                }

            }
        }
    }


    public List<Feld> felderMitWertFinden(int wert) {
        List<Feld> gefundene_felder = new ArrayList<Feld>();
        for( int y=0; y < STANDARD_HOEHE; y++) {
            for ( int x=0; x < STANDARD_BREITE; x++ ) {
                if( this.zahlenFeld[x][y].getWert() == wert ) {
                    gefundene_felder.add(this.zahlenFeld[x][y]);
                }
            }
        }
        return gefundene_felder;
    }

    public void felderMitIdFarbeSetzen(int id, Farbe farbe) {
        List<Feld> gefundene_felder = new ArrayList<Feld>();
        for( int y=0; y < STANDARD_HOEHE; y++) {
            for ( int x=0; x < STANDARD_BREITE; x++ ) {
                if( this.zahlenFeld[x][y].getID() == id ) {
                    this.zahlenFeld[x][y].setFarbe(farbe);
                }
            }
        }
    }


    public List<Feld> alleFelder() {


        long start = System.currentTimeMillis();
        List<Feld> gefundene_felder = new ArrayList<Feld>();
        for( int y=0; y < STANDARD_HOEHE; y++) {
            for ( int x=0; x < STANDARD_BREITE; x++ ) {
                    gefundene_felder.add(this.zahlenFeld[x][y]);
            }
        }

        long start2 =  System.currentTimeMillis();

        List<Feld> colletortest = Arrays.stream(this.zahlenFeld)
                .flatMap(Arrays::stream)
                .collect(Collectors.toList());

        long stop =  System.currentTimeMillis();
        System.err.println("For Loops: "+(start2-start)+"ms Streaming: "+(stop-start2)+"ms");

        return gefundene_felder;
    }



    public List<Grenzlinie> grenzlinienMitFarbe(List<Feld> felder_mit_wert, Farbe farbe ) {
        List<Grenzlinie> horizontale_treffer = new ArrayList<Grenzlinie>();
        List<Grenzlinie> vertikale_treffer = new ArrayList<Grenzlinie>();
        // blöderweise langsam --> hashmap für wert -> Feld -> grenzlinien
        // vertikale grenzen
        for (int y = 0; y < STANDARD_HOEHE; y++) {
            for (int x = 0; x < STANDARD_BREITE + 1; x++) {
                if (felder_mit_wert.contains(vertikale_grenzen[x][y].getNachbar1()) || felder_mit_wert.contains(vertikale_grenzen[x][y].getNachbar2())) {
                    if( vertikale_grenzen[x][y].getFarbe() == farbe ) {
                        vertikale_treffer.add(vertikale_grenzen[x][y]);
                    }
                }
            }
        }
        // horizontale grenzen
        for (int y = 0; y < STANDARD_HOEHE + 1; y++) {
            for (int x = 0; x < STANDARD_BREITE; x++) {
                if (felder_mit_wert.contains(horizontale_grenzen[x][y].getNachbar1()) || felder_mit_wert.contains(horizontale_grenzen[x][y].getNachbar2())) {
                    if( horizontale_grenzen[x][y].getFarbe() == farbe ) {
                        horizontale_treffer.add(horizontale_grenzen[x][y]);
                    }
                }

            }
        }
        System.out.println("Vertikale Treffer: "+vertikale_treffer.size());
        System.out.println("Horizontale Treffer: "+horizontale_treffer.size());
        for( Grenzlinie gl : vertikale_treffer ) {
            System.out.println(gl.toString());
        }
        for( Grenzlinie gl : horizontale_treffer ) {
            System.out.println(gl.toString());
        }
       vertikale_treffer.addAll(horizontale_treffer);
        return new ArrayList<Grenzlinie>(vertikale_treffer) ;
    }

    public boolean setzeGrenzlinie( int id, Farbe farbe, boolean feldberechnen ) {
        // hier würde sich die hashmap auszahlen, außerdem macht die Trennung zwischen horizontal/vertikal sinn?
        // es kommt immer auf o(n^2) pro abfrage bei einer hashmap wären wir bei o(nlogn)

        // vertikale grenzen
        for( int y=0; y < STANDARD_HOEHE; y++) {
            for ( int x=0; x < STANDARD_BREITE+1; x++ ) {
              if( vertikale_grenzen[x][y].getId() == id ) {
                  vertikale_grenzen[x][y].setFarbe(farbe,reihenfolge);
                  if( feldberechnen ) { angrenzendeFelderBerechnen( vertikale_grenzen[x][y] );}
                  return true;
              }
            }
        }
        // horizontale grenzen
        for( int y=0; y < STANDARD_HOEHE+1; y++) {
            for ( int x=0; x < STANDARD_BREITE; x++ ) {
                if( horizontale_grenzen[x][y].getId() == id ) {
                    horizontale_grenzen[x][y].setFarbe(farbe,reihenfolge);
                    if( feldberechnen ) { angrenzendeFelderBerechnen( horizontale_grenzen[x][y] );}
                    return true;
                }
            }
        }
        return false;

    }

    public void angrenzendeFelderBerechnen( Grenzlinie grenze ) {
        if( grenze.getNachbar1() != null && grenze.getNachbar1().getFarbe() == Farbe.LEER ) {
            List<Grenzlinie> allegrenzen = alleGrenzenEinesFeldes(grenze.getNachbar1());
            boolean erfolg = true;
            for( Grenzlinie gl :allegrenzen ) {
                if( gl.getFarbe() == Farbe.LEER ) { erfolg = false; }
            }
            if( erfolg ) {
                felderMitIdFarbeSetzen( grenze.getNachbar1().getID(),grenze.getFarbe());
            }
        }

        if( grenze.getNachbar2() != null && grenze.getNachbar2().getFarbe() == Farbe.LEER ) {
            List<Grenzlinie> allegrenzen = alleGrenzenEinesFeldes(grenze.getNachbar2());
            boolean erfolg = true;
            for( Grenzlinie gl :allegrenzen ) {
                if( gl.getFarbe() == Farbe.LEER ) { erfolg = false; }
            }
            if( erfolg ) {
                felderMitIdFarbeSetzen( grenze.getNachbar2().getID(),grenze.getFarbe());
            }
        }






    }



    public List<Grenzlinie> alleGrenzenEinesFeldes( Feld feld ) {
        List<Grenzlinie> grenzen = new ArrayList<Grenzlinie>();
        for (int y = 0; y < STANDARD_HOEHE; y++) {
            for (int x = 0; x < STANDARD_BREITE + 1; x++) {
                if ((vertikale_grenzen[x][y].getNachbar1() == feld) ||
                        (vertikale_grenzen[x][y].getNachbar2() ==feld) ) {
                    grenzen.add(vertikale_grenzen[x][y]);
                }
            }
        }


        for (int y = 0; y < STANDARD_HOEHE + 1; y++) {
            for (int x = 0; x < STANDARD_BREITE; x++) {
                if ((horizontale_grenzen[x][y].getNachbar1() == feld) ||
                        (horizontale_grenzen[x][y].getNachbar2() == feld) ) {
                    grenzen.add(horizontale_grenzen[x][y]);
                }
            }
        }
        if( grenzen.size() != 4 ) {
            System.err.println("Falsche Grenzenanzahl für Feld: "+feld.getID());
        }
        return grenzen;
    }


    private int zufallsDoppelWuerfel() {
        return ( (zufall.nextInt(6 ) + 1)*(zufall.nextInt(6 ) + 1) );
    }

    // fläche
    // grenzen

    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String VERTICAL_BORDER = "|";


    private String ansiFarbe( Farbe farbe ) {
        String color = ANSI_RESET;
        switch( farbe ) {
            case ROT:color=ANSI_RED;break;
            case GRUEN:color=ANSI_GREEN;break;
        }
        return color;
    }


    public void zeigeSpielfeld(PrintStream ausgabe) {

        for( int y=0; y < STANDARD_HOEHE; y++) {

            for ( int x=0; x < STANDARD_BREITE; x++ ) {
                ausgabe.print(ANSI_RESET+"+"+ ansiFarbe(horizontale_grenzen[x][y].getFarbe())+"-----"+ANSI_RESET );
            }
            ausgabe.println("+");


            for ( int x=0; x < STANDARD_BREITE; x++ ) {
                ausgabe.print( ansiFarbe(this.vertikale_grenzen[x][y].getFarbe()) + VERTICAL_BORDER + ANSI_RESET );
                // Feldwert
                ausgabe.print("  "+ansiFarbe(this.zahlenFeld[x][y].getFarbe()) + this.zahlenFeld[x][y].getWert() + ANSI_RESET +  (this.zahlenFeld[x][y].getWert()<10?"  ":" "));
            }
            // last border
            ausgabe.print( ansiFarbe(this.vertikale_grenzen[STANDARD_BREITE][y].getFarbe()) + VERTICAL_BORDER + ANSI_RESET );
            ausgabe.println();
        }
        for ( int x=0; x < STANDARD_BREITE; x++ ) {
            ausgabe.print(ANSI_RESET+"+"+ ansiFarbe(horizontale_grenzen[x][STANDARD_HOEHE].getFarbe())+"-----"+ANSI_RESET );
        }
        ausgabe.println("+");
    }
}

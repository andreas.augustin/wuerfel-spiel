package net.florian;

import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        System.out.println("Hallo Welt, hier ist Florians Würfelspiel.");
        WuerfelRechnenController rechner = new WuerfelRechnenController();
        rechner.initialisiereSpielfeld();
        rechner.zeigeSpielfeld(System.out);
        Scanner in = new Scanner(System.in);

        while(true) {

            boolean gueltige_zahl = false;
            String input=null;
            while( gueltige_zahl != true ) {
                System.out.print("Würfelzahl ["+rechner.gibAktuelleFarbe().name()+"]: ");
                input = in.nextLine();
                if (input.equals("ende")) {
                    System.exit(0);
                }
                if (input.equals("zeig")) {
                    rechner.zeigeSpielfeld(System.out);
                    continue;
                }
                if( input==null ) { continue; }
                if( input.length() < 1 ) {continue; }
                gueltige_zahl = rechner.gueltigeWuerfelZahl(Integer.parseInt(input.trim()) );
                if(!gueltige_zahl) {System.out.println("Bitte keine D&D Würfel verwenden.");}
            }

            List<Feld> felder = rechner.felderMitWertFinden( Integer.parseInt(input.trim()));
            if ( felder.size() > 0 ) {
                rechner.grenzlinienMitFarbe( felder, Farbe.LEER);
                System.out.print("Welche Grenze ["+rechner.gibAktuelleFarbe().name()+"]: ");
                String grenze = in.nextLine();
                boolean erfolg = rechner.setzeGrenzlinie( Integer.parseInt(grenze.trim()),rechner.gibAktuelleFarbe(),true );
                if( erfolg ) {
                    System.out.println("Setze Grenze ["+ grenze+"] mit "+rechner.gibAktuelleFarbe().name()  );
                } else {
                    System.err.println("Konnte Grenze nicht setzen. (Achtung Spielerwechsel, aktuell keine Neueingabe notwendig.");
                    //reset
                }
               rechner.grenzlinienMitFarbe( rechner.alleFelder(), rechner.gibAktuelleFarbe() );
                rechner.aendereAktuelleFarbe();
                System.out.println("");
            } else {
                System.out.println("Keine Felder mit <"+input+"> gefunden.");
            }


        }




    }
}

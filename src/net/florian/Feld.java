package net.florian;


public class Feld {

    private int x;
    private int y;
    private int id;
    private Farbe farbe;
    private int wert;
    public Feld(int id, int x, int y, int wert) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.wert = wert;
        this.farbe = Farbe.LEER;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Farbe getFarbe() {
        return farbe;
    }

    public void setFarbe(Farbe farbe) {
        this.farbe = farbe;
    }

    public int getWert() {
        return wert;
    }

    public int getID() { return id; }

    @Override
    public String toString() {
        return "Feld{" +
                "x=" + x +
                ", y=" + y +
                ", id=" + id +
                ", farbe=" + farbe +
                ", wert=" + wert +
                '}';
    }
}

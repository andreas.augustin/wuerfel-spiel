package net.florian;

public class Grenzlinie {
    private int id;
    private Feld nachbar1;
    private Feld nachbar2;
    private Farbe farbe;
    private int reihenfolge;

    public Grenzlinie(int id, Feld nachbar1, Feld nachbar2) {
        this.id = id;
        this.nachbar1 = nachbar1;
        this.nachbar2 = nachbar2;
        this.farbe = Farbe.LEER;
        this.reihenfolge = 0;
    }

    public int getId() {
        return id;
    }


    public Feld getNachbar1() {
        return nachbar1;
    }

    public void setNachbar1(Feld nachbar1) {
        this.nachbar1 = nachbar1;
    }

    public Feld getNachbar2() {
        return nachbar2;
    }

    public void setNachbar2(Feld nachbar2) {
        this.nachbar2 = nachbar2;
    }

    public Farbe getFarbe() {
        return farbe;
    }

    public void setFarbe(Farbe farbe, int reihenfolge ) {
        this.farbe = farbe;
        this.reihenfolge = reihenfolge;
    }

    @Override
    public String toString() {
        return "Grenzlinie{" +
                "id=" + id +
                ", n1=" + nachbar1 +
                ", n2=" + nachbar2 +
                ", farbe=" + farbe +
                ", reihe="+reihenfolge +
                '}';
    }
}
